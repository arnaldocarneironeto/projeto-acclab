package arnymal.arngames.com.mazehandler;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import arnymal.arngames.com.astarsolver.AStar;
import arnymal.arngames.com.astarsolver.State;
import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;

/**
 * Created by arnaldo on 28/02/2015.
 */

public class Tree
{
    private final Node[][] nodeList;
    private final int width;
    private final int height;
    private ArrayList<State> solution;

    public Tree(int width, int height)
    {
        this.height = height;
        this.width = width;
        this.nodeList = new Node[width][height];
        for(int i = 0; i < width; i ++)
        {
            for(int j = 0; j < height; j ++)
            {
                this.nodeList[i][j] = new Node(i, j);
            }
        }
        solution = null;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public Node getNode(int posX, int posY)
    {
        return this.nodeList[posX][posY];
    }

    public int getSolutionStepsCount()
    {
        return solution.size();
    }

    public boolean thereIsStillVertex()
    {
        boolean result = false;
        for(int posX = 0; result == false && posX < width; posX ++)
        {
            for(int posY = 0; result == false && posY < height; posY ++)
            {
                result |= this.getNode(posX, posY).isOnGraph() == false;
            }
        }
        return result;
    }

    public ArrayList<State> getSolution()
    {
        return solution;
    }

    public void generate(long seed)
    {
        Random random = new Random(seed);
        setFirstRandomNode(random);
        while(this.thereIsStillVertex() == true)
        {
            Node node;
            int posX;
            int posY;
            boolean hasNeighboursOnTree;
            do
            {
                do
                {
                    posX = random.nextInt(width);
                    posY = random.nextInt(height);
                    node = this.getNode(posX, posY);
                }
                while(node.isOnGraph());
                hasNeighboursOnTree = verifyNeighbours(node);
            }
            while(hasNeighboursOnTree == false);
            do
            {
                Node neighbour;
                int dir = random.nextInt(Direction.values().length);
                switch(dir)
                {
                    case 0: setEdge(node, Direction.UP, Direction.DOWN);
                        break;
                    case 1: setEdge(node, Direction.DOWN, Direction.UP);
                        break;
                    case 2: setEdge(node, Direction.LEFT, Direction.RIGHT);
                        break;
                    case 3: setEdge(node, Direction.RIGHT, Direction.LEFT);
                        break;
                }
            }
            while(node.isOnGraph() == false);
        }
        solve();
    }

    public void generate()
    {
        String seedString = InfoHolder.getInstance().getRandomSeed();
        Random random;
        if(seedString != null && seedString.isEmpty() == false)
        {
            random = new SecureRandom(ByteBuffer.allocate(4).putInt(seedString.hashCode()).array());
        }
        else
        {
            random = new SecureRandom();
        }
        generate(random.nextLong());
    }

    private void setEdge(Node node, Direction directionTo, Direction directionFrom)
    {
        Node neighbour;
        neighbour = getNeighbour(node, directionTo);
        if(neighbour != null && neighbour.isOnGraph())
        {
            node.setEdge(directionTo);
            node.setOnGraph();
            neighbour.setEdge(directionFrom);
        }
    }

    private void setFirstRandomNode(Random random)
    {
        int x = random.nextInt(width);
        int y = random.nextInt(height);
        this.nodeList[x][y].setOnGraph();
    }

    private boolean verifyNeighbours(Node node)
    {
        boolean hasNeighboursOnTree = false;
        hasNeighboursOnTree = verifiyNeighbourOnThisDirection(node, hasNeighboursOnTree, Direction.UP);
        hasNeighboursOnTree = verifiyNeighbourOnThisDirection(node, hasNeighboursOnTree, Direction.DOWN);
        hasNeighboursOnTree = verifiyNeighbourOnThisDirection(node, hasNeighboursOnTree, Direction.LEFT);
        hasNeighboursOnTree = verifiyNeighbourOnThisDirection(node, hasNeighboursOnTree, Direction.RIGHT);
        return hasNeighboursOnTree;
    }

    private boolean verifiyNeighbourOnThisDirection(Node node, boolean hasNeighboursOnTree, Direction direction)
    {
        Node neighbour;
        neighbour = getNeighbour(node, direction);
        hasNeighboursOnTree |= neighbour != null && neighbour.isOnGraph() == true;
        return hasNeighboursOnTree;
    }

    public Node getNeighbour(Node node, Direction dir)
    {
        Node result = null;
        int x = node.getPosition().x;
        int y = node.getPosition().y;
        switch(dir)
        {
            case UP:
                if(y > 0)
                {
                    result = this.getNode(x, y - 1);
                }
                break;
            case DOWN:
                if(y < height - 1)
                {
                    result = this.getNode(x, y + 1);
                }
                break;
            case RIGHT:
                if(x < width - 1)
                {
                    result = this.getNode(x + 1, y);
                }
                break;
            case LEFT:
                if(x > 0)
                {
                    result = this.getNode(x - 1, y);
                }
                break;
        }
        return result;
    }

    public void print(String filePath)
    {
        try
        {
            PrintWriter out = new PrintWriter(new File(filePath));
            String line;
            String subLine;
            String fullBlock = "\u2588";
            line = fullBlock;
            for(int i = 0; i < width; i ++)
            {
                line += fullBlock + fullBlock;
            }
            out.println(line);
            for(int j = 0; j < height; j ++)
            {
                line = fullBlock;
                subLine = fullBlock;
                for(int i = 0; i < width; i ++)
                {
                    line += " ";
                    Node node = this.getNode(i, j);
                    line += node.hasEdge(Direction.RIGHT) ? " " : fullBlock;
                    subLine += node.hasEdge(Direction.DOWN) ? " " : fullBlock;
                    subLine += fullBlock;
                }
                out.println(line);
                out.println(subLine);
            }
            out.flush();
        }
        catch(IOException ex)
        {
            Logger.getLogger(Tree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void drawOn(Canvas canvas, boolean hasFocus)
    {
        int cellSize = InfoHolder.getInstance().getCellSize();
        int thickness = getThickness(cellSize);
        canvas.drawColor(Color.LTGRAY);
        Paint paint = new Paint();

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
        int a = 0;
        int b = 0;
        canvas.drawRect(a * cellSize, b * cellSize, (a + 1) * cellSize, (b + 1) * cellSize, paint);

        paint.setColor(Color.GREEN);
        a = getWidth() - 1;
        b = getHeight() - 1;
        canvas.drawRect(a * cellSize, b * cellSize, (a + 1) * cellSize, (b + 1) * cellSize, paint);

        paint.setColor(hasFocus? Color.argb(255, 84, 54, 23): Color.argb(255, 84, 69, 54));

        ArrayList<Rect> walls = new ArrayList<>();
        for(int j = 0; j < getHeight(); j++)
        {
            for(int i = 0; i < getWidth(); i++)
            {
                Node node = getNode(i, j);
                createAndDrawWall(canvas, paint, walls, node, Direction.UP, i * cellSize, (i + 1) * cellSize, j * cellSize, j * cellSize + thickness);
                createAndDrawWall(canvas, paint, walls, node, Direction.DOWN, i * cellSize, (i + 1) * cellSize, (j + 1) * cellSize - thickness, (j + 1) * cellSize);
                createAndDrawWall(canvas, paint, walls, node, Direction.LEFT, i * cellSize, i * cellSize + thickness, j * cellSize, (j + 1) * cellSize);
                createAndDrawWall(canvas, paint, walls, node, Direction.RIGHT, (i + 1) * cellSize - thickness, (i + 1) * cellSize, j * cellSize, (j + 1) * cellSize);
            }
        }
        InfoHolder.getInstance().setWalls(walls.toArray(new Rect[0]));
    }

    private void createAndDrawWall(Canvas canvas, Paint paint, ArrayList<Rect> walls, Node node, Direction direction, int left, int right, int top, int bottom)
    {
        Rect rect;
        if(node.hasEdge(direction) == false)
        {
            rect = new Rect(left, top, right, bottom);
            canvas.drawRect(rect, paint);
            walls.add(rect);
        }
    }

    public void drawSolutionOn(Canvas canvas, boolean hasFocus)
    {
        solve();

        int cellSize = InfoHolder.getInstance().getCellSize();
        int thickness = getThickness(cellSize);
        Paint paint = new Paint();

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        int a;
        int b;
        for(State state: solution)
        {
            a = state.getPosition().x;
            b = state.getPosition().y;
            canvas.drawRect(a * cellSize + 2 * thickness, b * cellSize + 2 * thickness, (a + 1) * cellSize - 2 * thickness, (b + 1) * cellSize - 2 * thickness, paint);
        }
    }

    private void solve()
    {
        if(this.solution == null)
        {
            Point startPoint = new Point(0, 0);
            Point endPoint = new Point(getWidth() - 1, getHeight() - 1);
            State startState = new State(null, startPoint, 0, startPoint.distanceTo(endPoint));
            State endState = new State(null, endPoint, startPoint.distanceTo(endPoint), startPoint.distanceTo(endPoint));
            AStar aStar = new AStar(startState, endState, this);
            this.solution = aStar.findSolution();
        }
    }

    private static int getThickness(int cellSize)
    {
        return cellSize / 8 > 0? cellSize / 8: 1;
    }
}
package arnymal.arngames.com.mazehandler;

/**
 * Created by arnaldo on 28/02/2015.
 */
public class Node
{
    private boolean onGraph;
    private final boolean[] edges;
    private Point position;

    public Node(int x, int y)
    {
        this.onGraph = false;
        this.edges = new boolean[Direction.values().length];
        for (boolean bool : this.edges)
        {
            bool = false;
        }
        this.position = new Point(x, y);
    }

    public boolean isOnGraph()
    {
        return this.onGraph;
    }

    public void setOnGraph()
    {
        this.onGraph = true;
    }

    public boolean hasEdge(Direction dir)
    {
        return this.edges[dir.getIndex()];
    }

    public void setEdge(Direction dir)
    {
        this.edges[dir.getIndex()] = true;
    }

    public Point getPosition()
    {
        return position;
    }

    public void setPosition(Point position)
    {
        this.position = position;
    }

    public void setPosition(int posX, int posY)
    {
        this.position = new Point(posX, posY);
    }
}
package arnymal.arngames.com.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.edu4java.android.killthemall.Sprite;

import arnymal.arngames.com.mazehandler.Tree;
import arnymal.arngames.com.projetoacclab.R;
import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;

/**
 * Created by arnaldo on 26/04/2015.
 */
public class MazeView extends ImageView
{
    private Sprite sprite;

    public MazeView(Context context)
    {
        super(context);
        InfoHolder.getInstance().setMaze(new Tree(2, 2));
        InfoHolder.getInstance().setCellSize(16);
    }

    public MazeView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if(InfoHolder.getInstance().getMaze() != null)
        {
            InfoHolder.getInstance().getMaze().drawOn(canvas, this.hasFocus());
            //InfoHolder.getInstance().getMaze().drawSolutionOn(canvas, this.hasFocus());
            if(sprite == null)
            {
                //createSprite(R.drawable.x, 4, 3);
                createSprite(R.drawable.mouse, 4, 3);
                //createSprite(R.drawable.sorceress, 4, 8);
                InfoHolder.getInstance().setMazeView(this);
            }
            sprite.drawOn(canvas);
        }
        else
        {
            Paint paint = new Paint();
            canvas.drawText("null", getWidth() / 2, getHeight() / 2, paint);
        }
    }

    private void createSprite(int drawable, int xDivisions, int yDivisions)
    {
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), drawable);
        this.sprite = new Sprite(bmp, xDivisions, yDivisions);
    }

    public Sprite getSprite()
    {
        return sprite;
    }
}
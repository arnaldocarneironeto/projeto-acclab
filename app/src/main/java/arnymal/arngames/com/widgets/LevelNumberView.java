package arnymal.arngames.com.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;

/**
 * Created by arnaldo on 25/04/2015.
 */
public class LevelNumberView extends View
{
    public LevelNumberView(Context context)
    {
        super(context);
    }

    public LevelNumberView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(getSuggestedMinimumWidth(), getSuggestedMinimumHeight());
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        drawOn(canvas, hasFocus());
    }

    public void drawOn(Canvas canvas, boolean hasFocus)
    {
        int width = getWidth();
        int height = getHeight();

        canvas.drawColor(Color.LTGRAY);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setSubpixelText(true);
        paint.setTextAlign(Paint.Align.CENTER);

        TextView textView = new TextView(getContext());
        String text = InfoHolder.getInstance().getNextLevel() + "";
        Rect bounds = new Rect();
        Paint textPaint = textView.getPaint();
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        int textWidth = bounds.width();
        int textHeight = bounds.height();
        float scale = Math.min(width / textWidth, height/ textHeight);

        canvas.save();
        canvas.translate((width - textWidth) / 2, 2 * (height - textHeight) / 3);
        canvas.scale(scale, scale);
        canvas.drawText(text, 0, 0, paint);
        canvas.restore();
    }
}
package arnymal.arngames.com.projetoacclab.threads;

import com.edu4java.android.killthemall.Sprite;

import java.util.TimerTask;

import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;
import arnymal.arngames.com.widgets.MazeView;

/**
 * Created by arnaldo on 01/07/2015.
 */
public class PhysicsTask extends TimerTask {
    @Override
    public void run() {
        MazeView mv = InfoHolder.getInstance().getMazeView();
        if(mv != null) {
            Sprite s = mv.getSprite();
            if(s != null) {
                s.update();
            }
        }
    }
}

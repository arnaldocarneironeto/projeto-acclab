package arnymal.arngames.com.projetoacclab;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import java.util.Calendar;
import java.util.Timer;

import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;
import arnymal.arngames.com.projetoacclab.threads.PhysicsTask;
import arnymal.arngames.com.projetoacclab.threads.RedrawTask;
import arnymal.arngames.com.widgets.MazeView;

public class PhaseActivity extends ActionBarActivity implements SensorEventListener
{
    private Sensor accelerometer;
    private SensorManager sm;
    private long startTime;
    private Timer redrawTimer;
    private Timer physicsTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phase);

        startTime = System.currentTimeMillis();
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        getSupportActionBar().hide();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        redrawTimer = new Timer();
        physicsTimer = new Timer();

        MazeView view = (MazeView) findViewById(R.id.mazeView);
        RedrawTask x = new RedrawTask(view);

        redrawTimer.schedule(x, 0, 1000/30);
        physicsTimer.schedule(new PhysicsTask(), 0, 1000/120);
        InfoHolder.getInstance().setxCoord(0);
        InfoHolder.getInstance().setyCoord(0);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        this.sm.unregisterListener(this, this.accelerometer);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        double accX = event.values[0] / SensorManager.GRAVITY_EARTH;
        double accY = event.values[1] / SensorManager.GRAVITY_EARTH;
        double accZ = event.values[2] / SensorManager.GRAVITY_EARTH;
        double totAcc = Math.sqrt(sqr(accX) + sqr(accY) + sqr(accZ));
        double tiltX = -toDegree(Math.asin(accX / totAcc));
        double tiltY = toDegree(Math.asin(accY / totAcc));

        double maxAngle = 45.0;
        double maxSpeed = 0.5;
        tiltX = Math.max(-maxAngle, Math.min(maxAngle, tiltX));
        tiltY = Math.max(-maxAngle, Math.min(maxAngle, tiltY));

        InfoHolder.getInstance().setxSpeed(tiltX / maxAngle * maxSpeed);
        InfoHolder.getInstance().setySpeed(tiltY / maxAngle * maxSpeed);

        int xCoord = InfoHolder.getInstance().getxCoord();
        int yCoord = InfoHolder.getInstance().getyCoord();
        int level = InfoHolder.getInstance().getNextLevel();
        if((xCoord >= 2 * (level + 1)) && (yCoord >= 2 * (level + 1)))
        {
            this.sm.unregisterListener(this, this.accelerometer);
            InfoHolder.getInstance().setxCoord(0);
            InfoHolder.getInstance().setyCoord(0);
            winLevel();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    }

    private static double toDegree(double x)
    {
        return 90 * (x / 1.5);
    }

    private static double toRad(double x)
    {
        return (Math.PI / 2) * (x / 1.5);
    }

    private static double sqr(double x)
    {
        return x * x;
    }

    public void winLevel()
    {

        long endTime = Calendar.getInstance().getTimeInMillis();
        long delta = endTime - startTime;
        createStatisticsIntent(InfoHolder.getInstance().getSolutionStepsCount() * 1000, delta);
    }

    private void createStatisticsIntent(long paramTime, long totalTime)
    {
        Intent intent = new Intent(this, StatisticsActivity.class);
        InfoHolder playerInfoHolder = InfoHolder.getInstance();
        playerInfoHolder.setParamTime(paramTime);
        playerInfoHolder.setTotalTime(totalTime);
        InfoHolder.getInstance().setxCoord(0);
        InfoHolder.getInstance().setyCoord(0);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
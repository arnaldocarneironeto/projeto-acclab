package arnymal.arngames.com.projetoacclab;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class ConfigurationActivity extends ActionBarActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        SharedPreferences prefs = getSharedPreferences("acclabconfig", Context.MODE_PRIVATE);
        String leaderboardName = prefs.getString("leaderboardName", "Name");
        boolean alwaysNewLabirinth = prefs.getBoolean("alwaysNewLabirinth", true);
        boolean hasFixedSeed = prefs.getBoolean("hasFixedSeed", false);
        String seedString = prefs.getString("seedString", "");
        boolean enabledShadowPlay = prefs.getBoolean("isShadowPlayEnabled", false);

        EditText editLeaderboardName = (EditText) findViewById(R.id.editLeaderboardName);
        CheckBox alwaysNewLabirinthCheckBox = (CheckBox) findViewById(R.id.checkBoxAlwaysNewLabirinth);
        CheckBox fixedSeedCheckBox = (CheckBox) findViewById(R.id.checkBoxFixedSeed);
        EditText editSeedBox = (EditText) findViewById(R.id.editSeedString);
        CheckBox enabledShadowPlayCheckBox = (CheckBox) findViewById(R.id.checkBoxEnableShadowPlay);

        editLeaderboardName.setText(leaderboardName);
        alwaysNewLabirinthCheckBox.setChecked(alwaysNewLabirinth);
        fixedSeedCheckBox.setEnabled(alwaysNewLabirinth == false);
        fixedSeedCheckBox.setChecked(hasFixedSeed);
        editSeedBox.setEnabled(alwaysNewLabirinth == false);
        editSeedBox.setText(seedString);
        enabledShadowPlayCheckBox.setChecked(enabledShadowPlay);
    }

    public void toggleFixedSeedCheckBox(View view)
    {
        boolean alwaysNewLabirinth = ((CheckBox) findViewById(R.id.checkBoxAlwaysNewLabirinth)).isChecked();
        CheckBox fixedSeedCheckBox = (CheckBox) findViewById(R.id.checkBoxFixedSeed);
        EditText editSeedBox = (EditText) findViewById(R.id.editSeedString);
        fixedSeedCheckBox.setEnabled(alwaysNewLabirinth == false);
        editSeedBox.setEnabled(alwaysNewLabirinth == false);
    }

    public void buttonSaveClick(View view)
    {
        String leaderboardName = ((EditText) findViewById(R.id.editLeaderboardName)).getText().toString();
        boolean alwaysNewLabirinth = ((CheckBox) findViewById(R.id.checkBoxAlwaysNewLabirinth)).isChecked();
        boolean hasFixedSeed = ((CheckBox) findViewById(R.id.checkBoxFixedSeed)).isChecked();
        String seedString = ((EditText) findViewById(R.id.editSeedString)).getText().toString();
        boolean enabledShadowPlay = ((CheckBox) findViewById(R.id.checkBoxEnableShadowPlay)).isChecked();

        SharedPreferences prefs = getSharedPreferences("acclabconfig", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("leaderboardName", leaderboardName);
        editor.putBoolean("alwaysNewLabirinth", alwaysNewLabirinth);
        editor.putBoolean("hasFixedSeed", hasFixedSeed);
        editor.putString("seedString", seedString);
        editor.putBoolean("isShadowPlayEnabled", enabledShadowPlay);
        editor.apply();
    }
}
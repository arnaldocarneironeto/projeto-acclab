package arnymal.arngames.com.projetoacclab;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;

import java.util.Timer;

import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;
import arnymal.arngames.com.projetoacclab.threads.PhysicsTask;

public class MainActivity extends ActionBarActivity
{
    private Intent intent;
    private Account account;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        getAccount();

        /*acceleration = (TextView) findViewById(R.id.acceleration);
        acceleration.setText(this.account.name + "\n" + this.account.hashCode() + "\n" + this.account.type);*/
    }

    private void getAccount()
    {
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();
        if(list.length > 0)
        {
            this.account = list[0];
        }
        else
        {
            this.account = new Account("John Doe", "null");
        }
    }

    public void playGame(View view)
    {
        InfoHolder.getInstance().setNextLevel(1);
        createIntent(GameManagerActivity.class);
    }

    public void showConfig(View view)
    {
        createIntent(ConfigurationActivity.class);
    }

    public void showCredits(View view)
    {
        createIntent(CreditsActivity.class);
    }

    private void createIntent(Class aClass)
    {
        this.intent = new Intent(this, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(this.intent);
    }
}

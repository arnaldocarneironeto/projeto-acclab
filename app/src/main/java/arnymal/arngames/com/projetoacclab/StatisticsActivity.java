package arnymal.arngames.com.projetoacclab;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;

public class StatisticsActivity extends ActionBarActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        getSupportActionBar().hide();

        long paramTime = InfoHolder.getInstance().getParamTime();
        long totalTime = InfoHolder.getInstance().getTotalTime();

        Button playNextButton = (Button) findViewById(R.id.buttonPlayNext);
        TextView text = (TextView) findViewById(R.id.textView4);
        setGrade(paramTime, totalTime, playNextButton, text);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
    }

    private void setGrade(long paramTime, long totalTime, Button playNextButton, TextView text)
    {
        playNextButton.setEnabled(true);
        String grade = "Z";
        if(totalTime < paramTime / 14.0 * 13)
        {
            grade = "A+";
        }
        else if(totalTime < paramTime)
        {
            grade = "A";
        }
        else
        {
            double delta = paramTime / 14.0;
            if(totalTime < paramTime + delta)
            {
                grade = "A-";
            }
            else if(totalTime < paramTime + 2 * delta)
            {
                grade = "B+";
            }
            else if(totalTime < paramTime + 3 * delta)
            {
                grade = "B";
            }
            else if(totalTime < paramTime + 4 * delta)
            {
                grade = "B-";
            }
            else if(totalTime < paramTime + 5 * delta)
            {
                grade = "C+";
            }
            else if(totalTime < paramTime + 6 * delta)
            {
                grade = "C";
            }
            else if(totalTime < paramTime + 7 * delta)
            {
                grade = "C-";
            }
            else if(totalTime < paramTime + 8 * delta)
            {
                grade = "D+";
            }
            else if(totalTime < paramTime + 9 * delta)
            {
                grade = "D";
            }
            else if(totalTime < paramTime + 10 * delta)
            {
                grade = "D-";
            }
            else if(totalTime < paramTime + 11 * delta)
            {
                grade = "E+";
            }
            else if(totalTime < paramTime + 12 * delta)
            {
                grade = "E";
            }
            else if(totalTime < paramTime + 13 * delta)
            {
                grade = "E-";
            }
            else if(totalTime < paramTime + 14 * delta)
            {
                grade = "F+";
            }
            else
            {
                playNextButton.setEnabled(false);
                grade = "F";
            }
        }
        text.setText("Grade: " + grade + "\tTempo: " + totalTime + "\tParametro: " + paramTime + "\tSteps: " + InfoHolder.getInstance().getMaze().getSolutionStepsCount());
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void playNext(View view)
    {
        createGameManagerIntent(InfoHolder.getInstance().getNextLevel() + 1);
    }

    public void playLast(View view)
    {
        createGameManagerIntent(InfoHolder.getInstance().getNextLevel());
    }

    private void createGameManagerIntent(int levelNumber)
    {
        InfoHolder.getInstance().setxCoord(0);
        InfoHolder.getInstance().setyCoord(0);
        Intent intent = new Intent(this, GameManagerActivity.class);
        InfoHolder.getInstance().setNextLevel(levelNumber);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
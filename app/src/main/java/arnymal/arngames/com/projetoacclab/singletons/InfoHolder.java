package arnymal.arngames.com.projetoacclab.singletons;

import android.graphics.Rect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import arnymal.arngames.com.mazehandler.Tree;
import arnymal.arngames.com.widgets.MazeView;

/**
 * Created by arnaldo on 30/04/2015.
 */
public class InfoHolder
{
    private int nextLevel;
    private long paramTime;
    private long totalTime;
    private int xCoord;
    private int yCoord;
    private double xSpeed;
    private double ySpeed;
    private int cellSize;
    private MazeView mazeView;
    private ArrayList<Rect> walls;
    private Tree maze;
    private String randomSeed;

    private static final InfoHolder holder = new InfoHolder();

    private InfoHolder()
    {
    }

    public int getNextLevel()
    {
        return nextLevel;
    }

    public void setNextLevel(int nextLevel)
    {
        this.nextLevel = nextLevel;
    }

    public long getParamTime()
    {
        return paramTime;
    }

    public void setParamTime(long paramTime)
    {
        this.paramTime = paramTime;
    }

    public long getTotalTime()
    {
        return totalTime;
    }

    public void setTotalTime(long totalTime)
    {
        this.totalTime = totalTime;
    }

    public int getxCoord()
    {
        return xCoord;
    }

    public void setxCoord(int xCoord)
    {
        this.xCoord = xCoord;
    }

    public int getyCoord()
    {
        return yCoord;
    }

    public void setyCoord(int yCoord)
    {
        this.yCoord = yCoord;
    }

    public double getxSpeed()
    {
        return xSpeed;
    }

    public void setxSpeed(double xSpeed)
    {
        this.xSpeed = xSpeed;
    }

    public double getySpeed()
    {
        return ySpeed;
    }

    public void setySpeed(double ySpeed)
    {
        this.ySpeed = ySpeed;
    }

    public int getCellSize()
    {
        return cellSize;
    }

    public void setCellSize(int cellSize)
    {
        this.cellSize = cellSize;
    }

    public MazeView getMazeView()
    {
        return mazeView;
    }

    public void setMazeView(MazeView mazeView)
    {
        this.mazeView = mazeView;
    }

    public void setWalls(Rect... walls)
    {
        this.walls = new ArrayList<>();
        Collections.addAll(this.walls, walls);
    }

    public boolean collidesWithWalls(Rect rect)
    {
        boolean result = false;
        for (int i = 0; i < this.walls.size() && result == false; i++)
        {
            Rect wall = this.walls.get(i);
            result |= Rect.intersects(rect, wall);
        }
        return result;
    }

    public List<Rect> collidesWithWhichWalls(Rect rect)
    {
        List<Rect> result = new ArrayList<>();
        for (int i = 0; i < this.walls.size(); i++)
        {
            Rect wall = this.walls.get(i);
            if(Rect.intersects(rect, wall))
            {
                result.add(wall);
            }
        }
        return result;
    }

    public Tree getMaze()
    {
        return maze;
    }

    public void setMaze(Tree maze)
    {
        this.maze = maze;
    }

    public int getSolutionStepsCount()
    {
        return this.maze.getSolutionStepsCount();
    }

    public String getRandomSeed()
    {
        return randomSeed;
    }

    public void setRandomSeed(String randomSeed)
    {
        this.randomSeed = randomSeed;
    }

    public static InfoHolder getInstance()
    {
        return holder;
    }
}
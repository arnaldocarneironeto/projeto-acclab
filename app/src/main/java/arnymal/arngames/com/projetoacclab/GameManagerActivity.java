package arnymal.arngames.com.projetoacclab;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;

import java.security.SecureRandom;
import java.util.Random;

import arnymal.arngames.com.mazehandler.Tree;
import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;
import arnymal.arngames.com.widgets.LevelNumberView;

public class GameManagerActivity extends ActionBarActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_manager);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        if(alwaysGenerateANewSeed() == false)
        {
            String newSeed;
            SharedPreferences prefs = getSharedPreferences("acclabconfig", Context.MODE_PRIVATE);
            boolean hasFixedSeed = prefs.getBoolean("hasFixedSeed", false);
            if(hasFixedSeed == false)
            {
                newSeed = getRandomSeed();
            }
            else
            {
                newSeed = prefs.getString("seedString", "");
                if(newSeed == null || newSeed.isEmpty())
                {
                    newSeed = "default";
                }
            }
            setNewSeed(newSeed);
        }
    }

    private void setNewSeed(String newSeed)
    {
        InfoHolder.getInstance().setRandomSeed(newSeed);
        SharedPreferences prefs = getPreferences(Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("seedString", newSeed);
        editor.apply();
    }

    public void playNextPhase(View view)
    {
        LevelNumberView levelNumberView = (LevelNumberView) findViewById(R.id.levelNumberView);
        int minValue = Math.min(levelNumberView.getWidth(), levelNumberView.getHeight());
        int cellSize = minValue / (2 * (InfoHolder.getInstance().getNextLevel() + 1) + 1);
        InfoHolder.getInstance().setMaze(new Tree(minValue / cellSize, minValue / cellSize));
        if(alwaysGenerateANewSeed() == true)
        {
            setNewSeed("");
        }
        InfoHolder.getInstance().getMaze().generate();
        InfoHolder.getInstance().setCellSize(cellSize);

        Intent intent = new Intent(this, PhaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private boolean alwaysGenerateANewSeed()
    {
        SharedPreferences prefs = getSharedPreferences("acclabconfig", Context.MODE_PRIVATE);
        return prefs.getBoolean("alwaysNewLabirinth", true);
    }

    public String getRandomSeed()
    {
        String result = "";
        Random random = new SecureRandom();
        for(int i = 0; i < 20; i++)
        {
            result += Integer.toHexString(random.nextInt());
        }
        return result;
    }
}
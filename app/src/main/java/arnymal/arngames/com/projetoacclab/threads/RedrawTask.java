package arnymal.arngames.com.projetoacclab.threads;

import java.util.TimerTask;

import arnymal.arngames.com.projetoacclab.R;
import arnymal.arngames.com.widgets.MazeView;

/**
 * Created by arnaldo on 01/07/2015.
 */
public class RedrawTask extends TimerTask {

    private MazeView mazeView;

    public RedrawTask(MazeView mazeView) {
        this.mazeView = mazeView;
    }

    @Override
    public void run() {
        mazeView.postInvalidate();
    }
}

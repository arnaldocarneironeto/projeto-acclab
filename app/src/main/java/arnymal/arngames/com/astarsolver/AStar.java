package arnymal.arngames.com.astarsolver;

import java.util.ArrayList;

import arnymal.arngames.com.mazehandler.Direction;
import arnymal.arngames.com.mazehandler.Node;
import arnymal.arngames.com.mazehandler.Point;
import arnymal.arngames.com.mazehandler.Tree;

/**
 * Created by arnaldo on 25/04/2015.
 */
public class AStar
{
    private final ArrayList<State> toVisit;
    private final State finalState;
    private final Tree map;

    public AStar(State initialState, State finalState, Tree map)
    {
        this.finalState = finalState;
        this.map = map;
        this.toVisit = new ArrayList<>();
        this.toVisit.add(initialState);
    }

    public ArrayList<State> findSolution()
    {
        ArrayList<State> solution = new ArrayList<>();
        while(this.toVisit.isEmpty() == false && solution.isEmpty() == true)
        {
            State state = this.toVisit.remove(0);
            if(state.isNotSelfAncestor() == true)
            {
                if(state.equals(this.finalState))
                {
                    State st = state;
                    do
                    {
                        solution.add(st);
                        st = st.getParent();
                    }
                    while(st != null);
                }
                else
                {
                    addChildren(state);
                }
            }
        }
        return solution;
    }

    private void addChildren(State state)
    {
        int distanceToGetHere = state.getDistanceToGetHere();
        Point pos = state.getPosition();
        Node node = this.map.getNode(pos.x, pos.y);
        for(Direction d: Direction.values())
        {
            if(node.hasEdge(d))
            {
                State childState = getChildState(state, d, distanceToGetHere);
                insertInTheRightPosition(childState);
            }
        }
    }

    private void insertInTheRightPosition(State state)
    {
        if(this.toVisit.isEmpty())
        {
            this.toVisit.add(state);
        }
        else
        {
            insertInTheRightPosition(state, 0, this.toVisit.size() - 1);
        }
    }

    private void insertInTheRightPosition(State state, int min, int max)
    {
        int mid = (min + max) / 2;
        if(state.getPredictedTotalDistance() == this.toVisit.get(mid).getPredictedTotalDistance())
        {
            this.toVisit.add(mid, state);
        }
        else if(min == max)
        {
            insertWhenThereIsJustOneElementToCompare(state, mid);
        }
        else if(max - min == 1)
        {
            insertWhenThereAreTwoElementsToCompare(state, min, max);
        }
        else if(state.getPredictedTotalDistance() < this.toVisit.get(mid).getPredictedTotalDistance())
        {
            insertInTheRightPosition(state, min, mid);
        }
        else
        {
            insertInTheRightPosition(state, mid, max);
        }
    }

    private void insertWhenThereIsJustOneElementToCompare(State state, int mid)
    {
        if(state.getPredictedTotalDistance() < this.toVisit.get(mid).getPredictedTotalDistance())
        {
            this.toVisit.add(mid, state);
        }
        else
        {
            if(mid + 1 < this.toVisit.size())
            {
                this.toVisit.add(mid + 1, state);
            }
            else
            {
                this.toVisit.add(state);
            }
        }
    }

    private void insertWhenThereAreTwoElementsToCompare(State state, int min, int max)
    {
        if(state.getPredictedTotalDistance() < this.toVisit.get(min).getPredictedTotalDistance())
        {
            this.toVisit.add(min, state);
        }
        else if(this.toVisit.get(max).getPredictedTotalDistance() < state.getPredictedTotalDistance())
        {
            if(max + 1 < this.toVisit.size())
            {
                this.toVisit.add(max + 1, state);
            }
            else
            {
                this.toVisit.add(state);
            }
        }
        else
        {
            this.toVisit.add(max, state);
        }
    }

    private State getChildState(State parent, Direction d, int distanceToGetHere)
    {
        Point position = new Point(parent.getPosition().x, parent.getPosition().y);
        switch(d)
        {
            case LEFT : position.translate(-1, 0);
                        break;
            case DOWN : position.translate(0, +1);
                        break;
            case RIGHT: position.translate(+1, 0);
                        break;
            case UP   : position.translate(0, -1);
                        break;
        }
        State result = new State(parent, position, distanceToGetHere + 1, position.distanceTo(this.finalState.getPosition()));
        return result;
    }
}
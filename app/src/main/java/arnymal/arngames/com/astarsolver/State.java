package arnymal.arngames.com.astarsolver;

import arnymal.arngames.com.mazehandler.Point;

/**
 * Created by arnaldo on 25/04/2015.
 */
public class State
{
    private final State parent;
    private final Point position;
    private final int distanceToGetHere;
    private final int predictedDistance;
    private final int predictedTotalDistance;

    public State(State parent, Point position, int distanceToGetHere, int predictedDistance)
    {
        this.parent = parent;
        this.position = position;
        this.distanceToGetHere = distanceToGetHere;
        this.predictedDistance = predictedDistance;
        this.predictedTotalDistance = distanceToGetHere + predictedDistance;
    }

    public State getParent()
    {
        return parent;
    }

    public Point getPosition()
    {
        return position;
    }

    public int getDistanceToGetHere()
    {
        return distanceToGetHere;
    }

    public int getPredictedTotalDistance()
    {
        return predictedTotalDistance;
    }

    public boolean isAncestor(State state)
    {
        boolean result = false;
        State possibleAncestor = state;
        while(possibleAncestor != null && result == false)
        {
            result = possibleAncestor.equals(this);
            possibleAncestor = possibleAncestor.getParent();
        }
        return result;
    }

    public boolean isNotSelfAncestor()
    {
        return this.isAncestor(this.getParent()) == false;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
        {
            return false;
        }
        if(getClass() != obj.getClass())
        {
            return false;
        }
        final State other = (State) obj;
        if(this.getPosition().equals(other.getPosition()) == false)
        {
            return false;
        }
        return true;
    }
}
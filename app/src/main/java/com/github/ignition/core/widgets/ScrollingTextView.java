package com.github.ignition.core.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;
import android.widget.TextView;

/**
 * A TextView that scrolls it contents across the screen, in a similar fashion as movie credits roll
 * across the theater screen.
 *
 * @author Matthias Kaeppler
 */
public class ScrollingTextView extends TextView implements Runnable
{
    private static final float DEFAULT_SPEED = 15.0f;

    private Scroller scroller;
    private float speed = DEFAULT_SPEED;
    private boolean continousScrolling = true;

    public ScrollingTextView(Context context)
    {
        super(context);
        setup(context);
    }

    public ScrollingTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setup(context);
    }

    private void setup(Context context)
    {
        scroller = new Scroller(context, new LinearInterpolator());
        setScroller(scroller);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);
        if(scroller.isFinished())
        {
            scroll();
        }
    }

    private void scroll()
    {
        int viewHeight = getHeight();
        int visibleHeight = viewHeight - getPaddingBottom() - getPaddingTop();
        int lineHeight = getLineHeight();

        int offset = -1 * visibleHeight;
        int totalLineHeight = getLineCount() * lineHeight;
        int distance = totalLineHeight + visibleHeight;
        int duration = (int) (distance * speed);

        if(totalLineHeight > visibleHeight)
        {
            scroller.startScroll(0, offset, 0, distance, duration);

            if(continousScrolling)
            {
                post(this);
            }
        }
    }

    @Override
    public void run()
    {
        if(scroller.isFinished())
        {
            scroll();
        }
        else
        {
            post(this);
        }
    }

    public void setSpeed(float speed)
    {
        this.speed = speed;
    }

    public float getSpeed()
    {
        return speed;
    }

    public boolean isContinousScrolling()
    {
        return continousScrolling;
    }

    public void setContinousScrolling(boolean continousScrolling)
    {
        this.continousScrolling = continousScrolling;
    }
}
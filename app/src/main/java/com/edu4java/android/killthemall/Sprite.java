package com.edu4java.android.killthemall;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import java.util.List;

import arnymal.arngames.com.projetoacclab.singletons.InfoHolder;

/**
 * Extracted from http://www.edu4java.com/en/androidgame/androidgame4.html
 * on 26/04/2015
 * Adapted by arnaldo on 27/04/2015.
 */
public class Sprite
{
    public static final float PRINT_FACTOR = 0.7f;
    // direction = 0 up, 1 left, 2 down, 3 right,
    // animation = 3 back, 1 left, 0 front, 2 right
    static int[] DIRECTION_TO_ANIMATION_MAP = {3, 1, 0, 2};
    private final int bmpRows;
    private final int bmpColumns;
    private int x;
    private int y;
    private int centerX;
    private int centerY;
    private Bitmap bmp;
    private int currentFrame;
    private int width;
    private int height;
    private float scale;

    public Sprite(Bitmap bmp, int bmpRows, int bmpColumns)
    {
        this.bmp = bmp;
        this.bmpRows = bmpRows;
        this.bmpColumns = bmpColumns;
        this.width = bmp.getWidth() / bmpColumns;
        this.height = bmp.getHeight() / bmpRows;
        this.currentFrame = 0;
        this.scale = 1.0f * InfoHolder.getInstance().getCellSize() / Math.max(this.width, this.height);
        this.x = this.y = (int) Math.max(InfoHolder.getInstance().getCellSize() * 0.17, 4);
    }

    public void update()
    {
        int cellSize = InfoHolder.getInstance().getCellSize();

        int width = (int) (this.width * this.scale * PRINT_FACTOR);
        int height = (int) (this.height * this.scale * PRINT_FACTOR);

        Rect boundingBox = new Rect(this.x, this.y, this.x + width, this.y + height);

        int divisoes = (int) Math.ceil(Math.max(Math.abs(InfoHolder.getInstance().getxSpeed()), Math.abs(InfoHolder.getInstance().getySpeed())));
        Rect rect;
        double dx = InfoHolder.getInstance().getxSpeed() * this.scale / divisoes;
        double dy = InfoHolder.getInstance().getySpeed() * this.scale / divisoes;
        int i = 0;
        int lastNonCollidingI = 0;
        boolean collides;
        do
        {
            i++;
            rect = createOffsetRect(boundingBox, (int) (dx * i), (int) (dy * i));
            collides = InfoHolder.getInstance().collidesWithWalls(rect);
            if(collides == false)
            {
                lastNonCollidingI = i;
            }
        }
        while(collides == false && i < divisoes);
        rect = createOffsetRect(boundingBox, (int) (dx * lastNonCollidingI), (int) (dy * lastNonCollidingI));
        this.x = rect.left;
        this.y = rect.top;

        /*
        Rect movedBoundingBoxHor = createOffsetRect(boundingBox, (int) (InfoHolder.getInstance().getxSpeed() * this.scale * PRINT_FACTOR), 0);
        this.x = checkCollisionAndGetNewPosition(movedBoundingBoxHor, this.x, InfoHolder.getInstance().getxSpeed(),  width, xCoord, cellSize, false);

        Rect movedBoundingBoxVer = createOffsetRect(boundingBox, 0, (int) (InfoHolder.getInstance().getySpeed() * this.scale * PRINT_FACTOR));
        this.y = checkCollisionAndGetNewPosition(movedBoundingBoxVer, this.y, InfoHolder.getInstance().getySpeed(), height, yCoord, cellSize, true);
        */

        this.centerX = this.x + width / 2;
        this.centerY = this.y + height / 2;
        InfoHolder.getInstance().setxCoord(this.centerX / cellSize);
        InfoHolder.getInstance().setyCoord(this.centerY / cellSize);

        this.currentFrame = ++this.currentFrame % this.bmpColumns;
    }

    private Rect createOffsetRect(Rect boundingBox, int dx, int dy)
    {
        Rect result;
        result = new Rect(boundingBox);
        result.offset(dx, dy);
        return result;
    }

    private double checkCollisionAndGetNewPosition(Rect movedBoundingBox, double value, double speed, double size, int coord, int cellSize, boolean isVertical)
    {
        List<Rect> walls = InfoHolder.getInstance().collidesWithWhichWalls(movedBoundingBox);

        boolean collide = false;

        for(Rect wall : walls) {
            if(wall != null &&
                    (collideVerticalAxis(movedBoundingBox, wall) == !isVertical
                     || collideHorizontalAxis(movedBoundingBox, wall) == isVertical))
            {
                collide = true;
                /*
                double newValue;
                if(speed < 0)
                {
                    newValue = (coord + 0.1) * cellSize + 1;
                }
                else
                {
                    newValue = (coord + 0.9) * cellSize - size - 1;
                }
                if(Math.abs(newValue - value) < cellSize / 4)
                {
                    value = newValue;
                }
                */
                break;

            }
        }

        return  collide ? value : value + speed;
    }

    private boolean collideVerticalAxis(Rect movedBoundingBox, Rect wall) {
        boolean test = (movedBoundingBox.right > wall.left | movedBoundingBox.left > wall.right);
        boolean result = movedBoundingBox.top > wall.bottom && test;
        result |= movedBoundingBox.bottom < wall.top && test;

        return !result;
    }

    private boolean collideHorizontalAxis(Rect movedBoundingBox, Rect wall) {
        boolean test = (movedBoundingBox.top < wall.bottom | movedBoundingBox.bottom < wall.top);
        boolean result = movedBoundingBox.right < wall.left && test;
        result |= movedBoundingBox.left > wall.right && test;

        return !result;
    }

    private double limitedSpeed(int cellSize, double number)
    {
        return Math.max(-cellSize, Math.min(cellSize, number)) * this.scale;
    }

    private double limitedSpeed2(int cellSize, double number)
    {
        //return Math.max(-cellSize / 2.0, Math.min(cellSize / 2.0, number));
        if(number > 0)
        {
            return 1 * this.scale;
        }
        else
        {
            return -1 * this.scale;
        }
    }

    public void drawOn(Canvas canvas)
    {
        float scale = this.scale * PRINT_FACTOR;
        int srcX = currentFrame * width;
        int srcY = getAnimationRow() * height;
        double centeredX = x;
        double centeredY = y;
        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
        canvas.save();
        canvas.scale(scale, scale);
        canvas.translate((float) ((1 - scale) * x / scale), (float) ((1 - scale) * y / scale));
        Rect dst = new Rect((int) centeredX, (int) centeredY, (int) (centeredX + width), (int) (centeredY + height));
        canvas.drawBitmap(bmp, src, dst, null);
        canvas.restore();
    }

    // direction = 0 up, 1 left, 2 down, 3 right,
    // animation = 3 back, 1 left, 0 front, 2 right
    private int getAnimationRow()
    {
        double dirDouble = (Math.atan2(InfoHolder.getInstance().getxSpeed(), InfoHolder.getInstance().getySpeed()) / (Math.PI / 2) + 2);
        int direction = (int) (Math.round(dirDouble)) % bmpRows;
        return DIRECTION_TO_ANIMATION_MAP[direction];
    }
}